//
//  main.m
//  GPUImageProject
//
//  Created by Jacob Gundersen on 5/2/12.
//  Copyright (c) 2012 Interrobang Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JGAppDelegate class]));
    }
}
